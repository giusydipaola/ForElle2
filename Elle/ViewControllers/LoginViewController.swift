//
//  LoginViewController.swift
//  Elle
//
//  Created by Giusy Di Paola on 21/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit
import AuthenticationServices

class LoginViewController: UIViewController,ASAuthorizationControllerDelegate {
    @IBOutlet weak var textLogin: UITextField!
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var buttonLogout: UIButton!
    @IBOutlet weak var viewAppleLogin: UIView!
    @IBOutlet weak var loginTitle: UILabel!
    @IBOutlet weak var dismissButtonX: UIButton!
    
    @IBOutlet weak var labelTop: UILabel!
    @IBOutlet weak var done: UIButton!
    
    @IBOutlet weak var Imagebackground: UIImageView!
    
    let border : UIColor = UIColor(red: 27.0/255.0, green: 29.0/255.0, blue: 34.0/255.0, alpha: 1.0 )
    var authorizationButton: ASAuthorizationAppleIDButton?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: HIDE
        
        textName.isHidden = true
        textLogin.isHidden = true
        done.isHidden = true
        
        //textName.attributedPlaceholder = "Type Your Name".toAttributed(alignment: .center)
        //textLogin.attributedPlaceholder = "Type Your Mobile Number".toAttributed(alignment: .center)
        
        //MARK: Design TxtField
        textName.layer.cornerRadius = 13.0
        textName.clipsToBounds = true
        textName.layer.borderColor = border.cgColor
        textName.layer.borderWidth = 1.0
        //--------------------------------------------------
        textLogin.layer.cornerRadius = 13.0
        textLogin.clipsToBounds = true
        textLogin.layer.borderColor = border.cgColor
        textLogin.layer.borderWidth = 1.0
        
        //MARK: DESIGN BUTTONS
        
        done.layer.masksToBounds = true
        done.layer.borderWidth = 0.2
        done.layer.cornerRadius = 25.0
        
        /*buttonLogout.layer.masksToBounds = true
         buttonLogout.layer.borderWidth = 0.2
         buttonLogout.layer.cornerRadius = 25.0*/
        
        // Do any additional setup after loading the view.
        //Keyboard Handler
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        //let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        //view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /* dismissButtonX.layer.masksToBounds = true
         dismissButtonX.layer.borderWidth = 0.2
         dismissButtonX.layer.cornerRadius = 21.0 */
        done.setTitle(NSLocalizedString("DONE_BUTTON", comment: ""), for: .normal)
        labelTop.isHidden = true
        loginTitle.text = "JOIN THE COMMUNITY"
        loginTitle.text = NSLocalizedString("JOIN", comment: "")
        //Design Button
        if authorizationButton == nil {
            setUpSignInAppleButton()
        }
        
        let app = AppDelegate.App
        if app.user.isLogged {
            completeProfile()
            
        } else {
            //buttonRegister.isHidden = false
            //buttonLogin.isHidden = false
            viewAppleLogin.isHidden = false
            authorizationButton?.isHidden = false
            buttonLogout.isHidden = true
            textLogin.isEnabled = true
        }
    }
    
    // DISMISS KEYBOARD
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    //-------------------------------------------------------
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        }
    }
    func setUpSignInAppleButton() {
        authorizationButton = ASAuthorizationAppleIDButton(type: .signIn, style: .whiteOutline)
        authorizationButton?.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)
        
        //authorizationButton.layer.cornerRadius = 25.0
        //authorizationButton?.layer.frame = CGRect(x: 0, y: 0, width: self.viewAppleLogin.bounds.width - self.viewAppleLogin.frame.origin.x, height: self.viewAppleLogin.bounds.height)
        authorizationButton?.layer.frame = self.textName.frame //CGRect(x: 0, y: 0, width: self.viewAppleLogin.bounds.width - self.viewAppleLogin.frame.origin.x, height: self.viewAppleLogin.bounds.height)
        authorizationButton?.layer.masksToBounds = true
        authorizationButton?.layer.borderColor = border.cgColor
        
        authorizationButton?.layer.masksToBounds = true
        authorizationButton?.layer.borderWidth = 0.2
        authorizationButton?.layer.cornerRadius = 25.0
        
        //Add button on some view or stack
        //let view = UIView()
        //view.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 1)
        authorizationButton?.frame = CGRect(x: self.view.safeAreaInsets.left + 35,
                                            y: viewAppleLogin.frame.origin.y,
                                            width:  self.view.frame.width - self.view.safeAreaInsets.left - 35 - self.view.safeAreaInsets.right - 35,
                                            height: self.viewAppleLogin.bounds.height)
        self.view.addSubview(authorizationButton!)
        //self.viewAppleLogin.addSubview(authorizationButton!)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func onClickDismissView(_ sender: Any) {
        if((self.presentingViewController) != nil){
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func whenDoneIsClicked(_ sender: Any) {
        var username = textLogin.text!
        let name = textName.text!
        let app = AppDelegate.App
        
        let wasEdit = (app.user.user.number.count > 0) ? true : false
        username = username.replacingOccurrences(of: " ", with: "")
        username = username.replacingOccurrences(of: "-", with: "")
        if name.count == 0 {
            Utils.ShowMessage(vc: self, title: NSLocalizedString("LOGIN_TITOLO_ALERT_MANDATORY", comment: ""), message: NSLocalizedString("LOGIN_MANDATORY_NAME", comment: "Name Field"))
            return
        }
        if username.count == 0 {
            Utils.ShowMessage(vc: self, title: NSLocalizedString("LOGIN_TITOLO_ALERT_MANDATORY", comment: ""), message: NSLocalizedString("LOGIN_MANDATORY_NUMBER", comment: "Number Field"))
            return
        }
        if name.count > 0  && username.count > 6 {
            
            done.isHidden = true
            buttonLogout.isHidden = false
            textLogin.isEnabled = false
            textName.isEnabled = false
        }
        
        if username.count <= 6 {
            Utils.ShowMessage(vc: self, title: NSLocalizedString("LOGIN_TITOLO_ALERT_MANDATORY", comment: ""), message: NSLocalizedString("LOGIN_MANDATORY_NUMBER", comment: "Number Field"))
            return
        }
        
        app.user.updateUser(name: name, number: username) { (success, error, user) in
            if success && user != nil {
                app.user.user = user!
                var timeOut = 2.0
                if wasEdit {
                    self.textLogin.text = username
                    self.textName.text = name
                    timeOut = 0
                } else {
                    self.labelTop.text = NSLocalizedString("CONGRATULATION", comment: "")
                    self.loginTitle.text = NSLocalizedString("FORELLER_SUCCESS", comment: "")
                    self.textLogin.text = username
                    self.textName.text = name
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + timeOut) {
                    self.dismiss(animated: true)
                }
            } else {
                print("Update USER %@",error)
                Utils.ShowMessage(vc: self, title: NSLocalizedString("ALERT_TITOLO_ERROR", comment: ""), message: NSLocalizedString("LOGIN_UPDATE_USER", comment: ""))
            }
        }
        
        
    }
    @IBAction func onClickLogout(_ sender: UIButton) {
        
        // showLogOut(title: "Logging Out", message: "Are you sure you want to logout?")
        showLogOut(title: NSLocalizedString("TITLE_LOGOUT", comment: ""), message: NSLocalizedString("COMMENT_LOGOUT", comment: ""))
    }
    
    @objc func handleAppleIdRequest() {
        let app = AppDelegate.App
        
        print("handleAppleIdRequest %@",app.sharedInfo.tokenDevice)
        #if targetEnvironment(simulator)
        if app.sharedInfo.tokenDevice.count == 0 {
            app.sharedInfo.tokenDevice = "SIMULATOR"
        }
        #else
        if app.sharedInfo.tokenDevice.count == 0 {
            Utils.ShowMessage(vc: self, title: NSLocalizedString("LOGIN_TITOLO_ALERT_MANDATORY", comment: ""), message: NSLocalizedString("LOGIN_MANDATORY_DEVICE", comment: "Device Field"))
            return
        }
        #endif
        
        
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName,.email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
        
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            let userToken = appleIDCredential.user
            
            var name =  textName.text! //(fullName?.givenName ?? "") + " " + (fullName?.familyName ?? "")
            var number  = textLogin.text!
            //            //textPassword.text = userToken
            if name.count == 0 && fullName != nil {
                name = fullName?.givenName ?? ""
            }
            number = number.replacingOccurrences(of: " ", with: "")
            number = number.replacingOccurrences(of: "-", with: "")
            
            
            print("User id is \(userIdentifier) \n Full Name is \(String(describing: fullName)) \n Email id is \(String(describing: email))")
            
            let app = AppDelegate.App
            
            var user = UserCreate()
            user.name = name
            user.password = userToken
            user.number = number
            user.username = userToken
            user.tokenDevice = app.sharedInfo.tokenDevice
            
            app.user.createUser(user: user) { (success, error) in
                if success {
                    DispatchQueue.main.async {
                        self.completeProfile() //self.dismiss(animated: true)
                    }
                } else {
                    print("Create USER %@",error)
                    Utils.ShowMessage(vc: self, title: NSLocalizedString("ALERT_TITOLO_ERROR", comment: ""), message: NSLocalizedString("LOGIN_CREATE_USER", comment: ""))
                }
            }
        }
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        print(error)
        //Utils.ShowMessage(vc: self, title: "Login Error", message: error.localizedDescription)
    }
    
    func completeProfile() {
        //questa immagine devo cambiarla (la mano) forse nn mi piace.
        
        Imagebackground.image = UIImage(named:"secondscreenlogin")
        authorizationButton?.isHidden = true
        buttonLogout.isHidden = false
        textName.text = ""
        textLogin.text = ""
        textName.isHidden = false
        textLogin.isHidden = false
        done.isHidden = false
        textName.isUserInteractionEnabled = true
        viewAppleLogin.isHidden = true
        textLogin.isEnabled = true
        
        
        //Si tratta di una modifica al profilo
        let app = AppDelegate.App
        textName.text = app.user.user.name
        textLogin.text = app.user.user.number
        loginTitle.text = NSLocalizedString("Registration", comment: "Logged")
        
        if app.user.user.number.count > 0 {
            labelTop.text = NSLocalizedString("LOGIN_EDIT", comment: "")
            labelTop.isHidden = false
            loginTitle.isHidden = true
        } else {
            labelTop.text = NSLocalizedString("SIGNED", comment: "")
            labelTop.isHidden = false
            loginTitle.isHidden = false
        }
        
       
        
        
    }
    
    func showLogOut(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("YES_ANSWER_LOGOUT", comment: ""), style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
            let app = AppDelegate.App
            app.user.logout()
            self.dismiss(animated: true)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("NO_ANSWER_LOGOUT", comment: ""), style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
