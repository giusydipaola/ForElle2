//
//  RequestDetailViewController.swift
//  Elle
//
//  Created by Andrea Garau on 22/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation
import RMQClient

class RequestDetailViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var callButton: CallButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var audioTableView: UITableView!
    @IBOutlet weak var updateLabel: UILabel!
    
    var isLoaded: Bool = false
    
    private var audioFragments: [AudioFragment]?
    private var userCoordinates: CLLocationCoordinate2D?
    
    var mqttConnection: RMQConnection?
    
    public var helpRequest: HelpRequestDataResponse? {
        didSet {
            if self.helpRequest != nil && isLoaded {
                DispatchQueue.main.async {
                    self.reloadData()
                }
            }
        }
    }
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        updateUI()
        
        audioTableView.tableFooterView = UIView()
        // Test the callButton setting a fake name
        callButton.calleeName = NSLocalizedString("CHIAMA", comment: "Call") + "\(self.helpRequest?.user?.name ?? "Supporter")"
        isLoaded = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotifyUpdateRequest), name: Notification.Name("notifyUpdateRequest"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onNotifyEndRequest), name: Notification.Name("notifyEndRequest"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Set the navigation bar title
        if let userName = self.helpRequest?.user?.name {
            let format = NSLocalizedString("RICHIESTA_DI", comment: "User's Request")
            self.title = String.localizedStringWithFormat(format, userName)
        }
        
        reloadData()
        activeRealTimeMessage()
        
        let requestApi = HelpRequestApi()
        
        if let obj = helpRequest {
            if obj.helpRequest.id > 0 {
                requestApi.find(id: obj.helpRequest.id) { (success, error, requestData) in
                    if success {
                        self.helpRequest = requestData
                    }
                }
            }
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        stopRealTimeMessage()
    }
    //Notifica chiamata all'arrivo di un nuovo messaggio audio
    @objc func onNotifyUpdateRequest(notification: Notification) {
        if notification.object != nil {
            let requestData = notification.object as? HelpRequestDataResponse
            if requestData != nil {
                print("onNotifyUpdateRequest")
                print(requestData!)
                DispatchQueue.main.async {
                    self.updateLabel.text = NSLocalizedString("AGGIORNANDO", comment: "Updating the request")
                    self.helpRequest = requestData!
                }
            }
        }
    }
    //Notifica chiamata alla fine di una richiesta
    @objc func onNotifyEndRequest(notification: Notification) {
        if notification.object != nil {
            let requestData = notification.object as? HelpRequestDataResponse
            if requestData != nil {
                print("onNotifyEndRequest")
                print(requestData!)
                DispatchQueue.main.async {
                    self.updateLabel.text = NSLocalizedString("SESSIONE_TERMINATA", comment: "La richiesta si è conclusa")
                    self.helpRequest = requestData!
                }
            }
        }
    }
    
    /// Draw the user path on the map
    /// It checks for every location received and draw the path using MKPolyline
    fileprivate func drawUserPathOnMap() {
        if let details = helpRequest?.helpRequestDetails {
            var locationsPolylinePoints = [CLLocationCoordinate2D]()
            for detail in details {
                locationsPolylinePoints.append(CLLocationCoordinate2D(latitude: detail.lat, longitude: detail.lon))
            }
            let locationsPolyline = MKPolyline(coordinates: locationsPolylinePoints, count: locationsPolylinePoints.count)
            mapView.addOverlay(locationsPolyline)
        }
    }
    
    func reloadData() {
        // Set last position of the user
        self.userCoordinates = CLLocationCoordinate2DMake(self.helpRequest!.helpRequest.lat, self.helpRequest!.helpRequest.lon)
        
        for helpRequestDetail in self.helpRequest!.helpRequestDetails.reversed() {
            if helpRequestDetail.lat.magnitude > 0 && helpRequestDetail.lon.magnitude > 0 {
                self.userCoordinates = CLLocationCoordinate2DMake(helpRequestDetail.lat, helpRequestDetail.lon)
                break
            }
        }
        self.showLocation(userCoordinates!)
        
        drawUserPathOnMap()

        // Create the array of audioFragments
        if let helpRequest = self.helpRequest {
            if audioFragments != nil {
                audioFragments!.removeAll()
            } else {
                self.audioFragments = []
            }
                        
            for helpRequestDetail in helpRequest.helpRequestDetails {
                if helpRequestDetail.audioFileUrl.count > 0 {
                    let audioFragment = AudioFragment(audioURL: URL(string: helpRequestDetail.getPublicUrl())!, creationDate: Utils.stringToDate(string: helpRequestDetail.dateInsert))
                    self.audioFragments!.append(audioFragment)
                }
            }
        }
        
        DispatchQueue.main.async {
            if let isActive = self.helpRequest?.helpRequest.active {
                if isActive >= 1 {
                    self.updateLabel.text = NSLocalizedString("AGGIORNANDO", comment: "Updating the request")
                } else {
                    self.updateLabel.text = NSLocalizedString("SESSIONE_TERMINATA", comment: "La richiesta si è conclusa")
                }
            }
        }
        
        self.audioTableView.reloadData()
    }
    
    private func updateUI() {
        mapView.layer.cornerRadius = 16
        mapView.region.span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
    }
    
    func activeRealTimeMessage() {
        if helpRequest?.helpRequest.active == 0 {
            return
        }
        guard let publishQueue = helpRequest?.helpRequest.publishQueue else {
            print("No PublishQueue present")
            return
        }
        if publishQueue.count == 0 {
            return
        }
        let api = HelpRequestApi()
        api.parameterMqtt { (success, error, dic) in
            if(success && dic != nil) {
                if(dic!.keys.contains("url")) {
                    if let url = dic!["url"] {
                        self.mqttConnection = RMQConnection(uri: url, delegate: RMQConnectionDelegateLogger())
                        self.mqttConnection!.start()
                        let ch = self.mqttConnection!.createChannel()
                        let x = ch.fanout(publishQueue)
                        let q = ch.queue("", options: .exclusive)
                        q.bind(x)
                        print("Waiting  message.")
                        q.subscribe({(_ message: RMQMessage) -> Void in
                            if let message = String(data: message.body, encoding: String.Encoding.utf8) {
                                print("Received \(message)")
                                if let messageObject = MqttNotify.parseObject(message: message) {
                                    if messageObject.isDetail {
                                        if let detail = messageObject.toDetail() {
                                            print(detail)
                                            self.helpRequest?.helpRequestDetails.append(detail)
                                            DispatchQueue.main.async {
                                                self.reloadData()
                                            }
                                        }
                                    } else if messageObject.isEnd {
                                        let requestApi = HelpRequestApi()
                                        if let detail = messageObject.toDetail() {
                                            DispatchQueue.main.async {
                                                requestApi.find(id: detail.helpRequestId) { (success, error, requestData) in
                                                    if success {
                                                        self.helpRequest = requestData
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        })
                    }
                    
                }
            } else {
                print(error)
            }
        }
    }
    func stopRealTimeMessage() {
        if self.mqttConnection != nil {
            self.mqttConnection?.close()
        }
    }
    //MARK: - IBActions
    @IBAction func callAction(_ sender: Any) {
        if let number = helpRequest?.user?.number {
            if let url = URL(string: "tel://\(number)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
}

extension RequestDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
}

extension RequestDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return audioFragments?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = audioTableView.dequeueReusableCell(withIdentifier: "audioCell", for: indexPath) as? AudioTableViewCell,
            let audioFragment = audioFragments?[indexPath.row] else {
            return UITableViewCell()
        }
        
        cell.configure(audioFragment: audioFragment)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let modalViewController = AudioPlayerViewController()
        modalViewController.transitioningDelegate = self
        modalViewController.modalPresentationStyle = .custom
        
        modalViewController.audioFragment = self.audioFragments?[indexPath.row]
        
        self.present(modalViewController, animated: true, completion: nil)
    }
}

extension RequestDetailViewController: MKMapViewDelegate {
    private func showLocation(_ userCoordinates: CLLocationCoordinate2D) {
        // Remove previous annotations
        let annotations = mapView.annotations
        mapView.removeAnnotations(annotations)
        
        // Add new annotation
        let dropPin = MKPointAnnotation()
        dropPin.coordinate = userCoordinates
        mapView.addAnnotation(dropPin)
        let animated = CLLocation.distance(from: mapView.centerCoordinate, to: userCoordinates) < 1000.0
        self.mapView.setCenter(userCoordinates, animated: animated)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRender = MKPolylineRenderer(overlay: overlay)
        polylineRender.strokeColor = UIColor.red.withAlphaComponent(0.5)
        polylineRender.lineWidth = 5
        
        return polylineRender        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }

        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)

        if annotationView == nil {
            annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)

            if let annotationPinView = annotationView as? MKMarkerAnnotationView {
                switch helpRequest?.helpRequest.serverity {
                case 1:
                    annotationPinView.markerTintColor = UIColor.systemGreen
                case 2:
                    annotationPinView.markerTintColor = UIColor.systemYellow
                default:
                    annotationPinView.markerTintColor = UIColor.systemRed
                }
                annotationView!.tintColorDidChange()
            }
        } else {
            annotationView!.annotation = annotation
        }

        return annotationView
    }
    

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let userCoordinates = userCoordinates, let openMapURL = URL(string: "http://maps.apple.com/?daddr=\(userCoordinates.latitude),\(userCoordinates.longitude)&z=10&t=s&dirflg=d") {
            if UIApplication.shared.canOpenURL(openMapURL) {
                UIApplication.shared.open(openMapURL, options: [:], completionHandler: nil)
            }
        }
    }
}

extension RequestDetailViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presenting)
    }
}
