//
//  ContactsViewController.swift
//  Elle
//
//  Created by Andrea Garau on 21/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit
import Contacts


class ContactsViewController: UIViewController{
    
    var cntName: String = ""
    var cntNumber: String = ""
    
    
    let app = AppDelegate.App
    let contactDefault = UserDefaults.standard
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var contactsSegmentedControl: UISegmentedControl!
    
    struct firstSection{
        var letter : String
        var contact : [UserResponse]
    }
    
    struct secondSection{
        let letter : String
        var contact : [UserResponse]
    }
    
    var contacts = [UserResponse]()
    var contactsNumbers = [String]()
    var contactsAddedMe = [UserResponse]()
    
    var contactsFirstSections = [firstSection]()
    var contactsSecondSection = [secondSection]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "Rectangle.png")
        backgroundImage.contentMode =  UIView.ContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
        
        tableView.tableFooterView = UIView()
        tableView.sectionIndexColor = UIColor(red: 0.745, green: 0.561, blue: 0.627, alpha: 1)
        
        contactsSegmentedControl.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .normal)
        contactsSegmentedControl.setTitle(NSLocalizedString("TITOLO_CONTROL_1", comment: "Your supporters"), forSegmentAt: 0)
        contactsSegmentedControl.setTitle(NSLocalizedString("TITOLO_CONTROL_2", comment: "Supported"), forSegmentAt: 1)
        
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.tableFooterView = UIView()
        app.user.loadFriends { (success, error) in
            if success{
                self.contacts = self.app.user.friends.friends
                DispatchQueue.main.async {
                    self.findMyFriends()
                    self.generateContactsFirstSection()
                    self.tableView.reloadData()
                }
            }
            else{
                print(error)
            }
        }
    }
    
    private func findMyFriends(){
        app.user.findMyFriends { (success, error, friendsfound) in
            DispatchQueue.main.async {
                if success{
                    if let friendsfound = friendsfound{
                        print("\(friendsfound.count) friends founded")
                        self.contactsAddedMe = friendsfound
                        self.generateContactsSecondSection()
                    }
                }
                else{
                    print(error)
                }
            }
        }
    }
    
    @IBAction func switchControl(_ sender: Any) {
        switch (contactsSegmentedControl.selectedSegmentIndex) {
        case 0:
            self.tableView.reloadData()
            break
        case 1:
            self.tableView.reloadData()
            break
        default:
            break
        }
    }
    
    func generateContactsFirstSection(){
        let groupedDictionary = Dictionary(grouping: contacts, by: { String($0.name.first ?? "#")})
        let keys = groupedDictionary.keys.sorted()
        contactsFirstSections = keys.map{firstSection(letter: $0, contact: groupedDictionary[$0]!) }
    }
    
    func generateContactsSecondSection(){
        let groupedDictionary = Dictionary(grouping: contactsAddedMe, by: {String($0.name.first ?? "#")})
        let keys = groupedDictionary.keys.sorted()
        contactsSecondSection = keys.map{secondSection(letter: $0, contact: groupedDictionary[$0]!) }
    }
      
}

extension ContactsViewController : UITableViewDelegate{
    
}

extension ContactsViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (contactsSegmentedControl.selectedSegmentIndex) {
        case 0:
            return contactsFirstSections[section].contact.count
        case 1:
            return contactsSecondSection[section].contact.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactTableViewCell
        
        switch (contactsSegmentedControl.selectedSegmentIndex) {
        case 0:
            let section = contactsFirstSections[indexPath.section]
            let contact = section.contact[indexPath.row]
            cell.nameLabel.text = contact.name
            cell.numberLabel.text = contact.number
            cell.selectionStyle = .none
            break
        case 1:
            let section = contactsSecondSection[indexPath.section]
            let contact = section.contact[indexPath.row]
            cell.nameLabel.text = contact.name
            cell.numberLabel.text = contact.number
            cell.selectionStyle = .none
            break
        default:
            break
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch (contactsSegmentedControl.selectedSegmentIndex) {
        case 0:
            return contactsFirstSections.count
        case 1:
            return contactsSecondSection.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch (contactsSegmentedControl.selectedSegmentIndex) {
        case 0:
            return contactsFirstSections[section].letter
        case 1:
            return contactsSecondSection[section].letter
        default:
            return ""
        }
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        switch (contactsSegmentedControl.selectedSegmentIndex) {
        case 0:
            return contactsFirstSections.map{$0.letter}
        case 1:
            return contactsSecondSection.map{$0.letter}
        default:
            return [""]
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = UIColor(red: 0.122, green: 0.129, blue: 0.149, alpha: 1)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor(red: 0.745, green: 0.561, blue: 0.627, alpha: 1)
    }
    
     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
         return true
     }

     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
         if (editingStyle == .delete) {
            tableView.beginUpdates()
            switch (contactsSegmentedControl.selectedSegmentIndex) {
            case 0:
                app.user.deleteFriend(friendId: contactsFirstSections[indexPath.section].contact[indexPath.row].id) { (success, error) in
                    if success{
                        print("Deleted friend with id: \(self.contactsFirstSections[indexPath.section].contact[indexPath.row].id)")
                        DispatchQueue.main.async {
                            self.contactsFirstSections[indexPath.section].contact.remove(at: indexPath.row)
                            self.tableView.deleteRows(at: [indexPath], with: .fade)
                            if self.contactsFirstSections[indexPath.section].contact.count == 0{
                                let indexSet = IndexSet(arrayLiteral: indexPath.section)
                                self.contactsFirstSections.remove(at: indexPath.section)
                                self.tableView.deleteSections(indexSet, with: .fade)
                            }
                            self.tableView.endUpdates()
                            
                        }
                    }
                    else{
                        print(error)
                    }
                }
                break
            case 1:
                app.user.deleteMyFriend(friendId: contactsSecondSection[indexPath.section].contact[indexPath.row].id) { (success, error, friends) in
                    if success{
                        print("Deleted friend with id: \(self.contactsSecondSection[indexPath.section].contact[indexPath.row].id)")
                        DispatchQueue.main.async {
                            self.contactsSecondSection[indexPath.section].contact.remove(at: indexPath.row)
                            self.tableView.deleteRows(at: [indexPath], with: .fade)
                            if self.contactsSecondSection[indexPath.section].contact.count == 0{
                                let indexSet = IndexSet(arrayLiteral: indexPath.section)
                                self.contactsSecondSection.remove(at: indexPath.section)
                                self.tableView.deleteSections(indexSet, with: .fade)
                            }
                            self.tableView.endUpdates()
                        }
                    }
                    else{
                        print(error)
                    }
                }
                break
            default:
                break
            }
            
         }
     }
}



