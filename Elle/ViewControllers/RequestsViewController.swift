//
//  RequestsViewController.swift
//  Elle
//
//  Created by Andrea Garau on 21/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit
import MapKit

class RequestsViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var requestsSegmentedControl: UISegmentedControl!
    
    //MARK: - Properties
    private let helpRequestAPI = HelpRequestApi()
    private let appDelegate = AppDelegate.App
    
    lazy var refreshControl = UIRefreshControl()
    
    public var helpRequests: [HelpRequestDataResponse]? {
        didSet {
            DispatchQueue.main.async {
                self.splitRequests()
                self.tableView.reloadData()
            }
        }
    }
    
    public var helpRequestsReceived: [HelpRequestDataResponse]? {
        didSet {
            self.isFooterViewHidden(!(self.helpRequestsReceived?.isEmpty ?? true))
        }
    }
    public var helpRequestsSent: [HelpRequestDataResponse]? {
        didSet {
            self.isFooterViewHidden(!(self.helpRequestsSent?.isEmpty ?? true))
        }
    }
    
    //MARK: - ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add footer
        let noRequestLabel = UILabel(frame: CGRect(x: 0, y: 15, width: tableView.frame.width, height: 30))
        noRequestLabel.text = NSLocalizedString("NO_REQUEST", comment: "Nessuna richiesta è stata ricevuta/inviata")
        noRequestLabel.textAlignment = .center
        noRequestLabel.textColor = UIColor(red: 0.745, green: 0.561, blue: 0.627, alpha: 1)
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        footerView.addSubview(noRequestLabel)
        tableView.tableFooterView = footerView
        
        // Update segmented control colors and set the action when the state changes
        requestsSegmentedControl.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .normal)
        requestsSegmentedControl.setTitle(NSLocalizedString("RICEVUTE", comment: "Richieste ricevute"), forSegmentAt: 0)
        requestsSegmentedControl.setTitle(NSLocalizedString("INVIATE", comment: "Richieste inviate"), forSegmentAt: 1)
        requestsSegmentedControl.addTarget(self, action: #selector(segmentedControllerIsSwitched(sender:)), for: .valueChanged)
        
        // Add refresh Controller
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(searchHelpRequests(sender:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchHelpRequests(sender: self)
    }
    
    /// Split the entire dataset of requests into sent requests and received ones
    private func splitRequests() {
        self.helpRequestsSent = self.helpRequests?.filter({ (helpRequest) -> Bool in
            if helpRequest.user?.number == self.appDelegate.user.user.number {
                return true
            }
            return false
        })
        
        self.helpRequestsReceived = self.helpRequests?.filter({ (helpRequest) -> Bool in
            if helpRequest.user?.number == self.appDelegate.user.user.number {
                return false
            }
            return true
        })
    }
    
    /// Called when the segmented controller change its state
    @objc func segmentedControllerIsSwitched(sender: AnyObject) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    /// Search for help requests associated to the user
    /// Includes both received and sent ones
    @objc func searchHelpRequests(sender: AnyObject) {
        refreshControl.endRefreshing()
        
        helpRequestAPI.search { (flag, errorDescription, helpRequestsResponse) in
            guard flag else {
                print(errorDescription)
                return
            }
            
            guard let helpRequests = helpRequestsResponse else {
                self.helpRequests = [HelpRequestDataResponse]()
                return
            }
            
            self.helpRequests = helpRequests
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    /// Hide footerView
    private func isFooterViewHidden(_ isHidden: Bool) {
        DispatchQueue.main.async {
            self.tableView.tableFooterView?.isHidden = isHidden
        }
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Transition to the next view, passing the helpRequest
        if segue.identifier == "toDetailRequest" {
            if let destinationVC = segue.destination as? RequestDetailViewController {
                if let helpRequest = sender as? HelpRequestDataResponse {
                    destinationVC.helpRequest = helpRequest
                }
            }
        }
    }
}


//MARK: - UITableViewDelegate
extension RequestsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let helpRequest: HelpRequestDataResponse?
        switch requestsSegmentedControl.selectedSegmentIndex {
        case 0:
            helpRequest = helpRequestsReceived?[indexPath.row]
        case 1:
            helpRequest = helpRequestsSent?[indexPath.row]
        default:
            helpRequest = nil
        }
        
        guard let detailRequest = helpRequest else { return }
        
        self.performSegue(withIdentifier: "toDetailRequest", sender: detailRequest)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            //Delete request
            switch requestsSegmentedControl.selectedSegmentIndex {
            case 0:
                if let helpRequestId = helpRequestsReceived?[indexPath.row].helpRequest.id {
                    tableView.beginUpdates()
                    let api = HelpRequestApi()
                    api.deleteRequest(helpRequestId: helpRequestId) { (success, error) in
                        DispatchQueue.main.async {
                            self.helpRequestsReceived?.remove(at: indexPath.row)
                            self.helpRequests = self.helpRequests?.filter({ (helpRequest) -> Bool in
                                return helpRequest.helpRequest.id == helpRequestId ? false : true
                            })
                            self.tableView.deleteRows(at: [indexPath], with: .fade)
                            //tableView.reloadData()
                            self.tableView.endUpdates()
                        }
                    }
                }
            case 1:
                if let helpRequestId = helpRequestsSent?[indexPath.row].helpRequest.id {
                    tableView.beginUpdates()
                    let api = HelpRequestApi()
                    api.deleteRequest(helpRequestId: helpRequestId) { (success, error) in
                        DispatchQueue.main.async {
                            self.helpRequestsSent?.remove(at: indexPath.row)
                            self.helpRequests = self.helpRequests?.filter({ (helpRequest) -> Bool in
                                return helpRequest.helpRequest.id == helpRequestId ? false : true
                            })
                            self.tableView.deleteRows(at: [indexPath], with: .fade)
                            //tableView.reloadData()
                            self.tableView.endUpdates()
                        }
                    }
                }
            default:
                return
            }
        }
    }
}


//MARK: - UITableViewDataSource
extension RequestsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Calculate the number of rows based on the user selection in the segmented controller
        switch requestsSegmentedControl.selectedSegmentIndex {
        case 0:
            return helpRequestsReceived?.count ?? 0
        case 1:
            return helpRequestsSent?.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "requestCell", for: indexPath) as? RequestTableViewCell
            else {
                return UITableViewCell()
        }
        
        cell.mapView.delegate = cell
        
        switch requestsSegmentedControl.selectedSegmentIndex {
        case 0:
            if let helpRequestDataResponse = helpRequestsReceived?[indexPath.row] {
                cell.setHelpRequest(helpRequestDataResponse.helpRequest, requesterName: helpRequestDataResponse.user?.name)
            } else {
                return UITableViewCell()
            }
        case 1:
            if let helpRequestDataResponse = helpRequestsSent?[indexPath.row] {
                cell.setHelpRequest(helpRequestDataResponse.helpRequest, requesterName: helpRequestDataResponse.user?.name)
            } else {
                return UITableViewCell()
            }
        default:
            return UITableViewCell()
        }
        
        return cell
    }
    
    
}
