//
//  TutorialViewController.swift
//  Elle
//
//  Created by Giusy Di Paola on 21/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit
import AVFoundation

class TutorialViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var view2Logo: UIView!
    
    @IBOutlet weak var explore: UIButton!
    
    var player: AVPlayer?
    var slides:[Slide] = [];
    let app = AppDelegate.App
    var userDef = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // View2Logo Design
       // view2Logo.layer.shadowColor = UIColor.white.cgColor
        //view2Logo.layer.shadowOpacity = 9
        
        //view2Logo.layer.shadowRadius = 8
        
        //view2Logo.alpha = CGFloat(2)
        scrollView.delegate = self
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        scrollView.isExclusiveTouch = true
        self.scrollView.canCancelContentTouches = true
        self.scrollView.delaysContentTouches = true
        view.bringSubviewToFront(pageControl)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*videoView.layer.cornerRadius = 0.0
        videoView.layer.masksToBounds = true
        videoView.layer.borderWidth = 0.2*/
        skipButton.layer.cornerRadius = 25.0
        skipButton.layer.masksToBounds = true
        skipButton.layer.borderWidth = 0.2
        //playBackgroundVideo()
        
        explore.layer.cornerRadius = 25.0
        explore.layer.masksToBounds = true
        explore.layer.borderWidth = 0.2
        explore.isHidden = true
        //UserDefaults
        if userDef == true {
            if UserDefaults.standard.bool(forKey: "tutorialAccepted") {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                skipButton.isHidden = true
                explore.isHidden = true
                let rootViewController = storyboard.instantiateViewController(withIdentifier: "SecondVC")
                //videoView.removeFromSuperview()
                //view2Logo.isHidden = true
                AppDelegate.App.window?.rootViewController = rootViewController
                self.dismiss(animated: false, completion: nil)
            }
        } else {
            skipButton.isHidden = true
            //explore.isHidden = true
        }
    }
    
    // Play Background Video
    private func playBackgroundVideo() {
        if let filePath = Bundle.main.path(forResource: "intro2", ofType:"mov") {
            let filePathUrl = NSURL.fileURL(withPath: filePath)
            player = AVPlayer(url: filePathUrl)
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.videoView.bounds
            playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: nil) { (_) in
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
            }
            self.videoView.layer.addSublayer(playerLayer)
            
            player?.play()
        }
    }
    func createSlides() -> [Slide] {
        
        let slide1:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide1.backImage.image = UIImage(named: "slide1")
        slide1.labelTitle.text = "Welcome"
        slide1.iconTop.image = UIImage(named: "1c")
        slide1.labelDescription.text = "Feel free to skip this tutorial."
        slide1.labelTitle.text = NSLocalizedString("Welcome", comment: "slide 1")
        slide1.labelDescription.text = NSLocalizedString("Feelfree", comment: "slide 1")
        
        let slide2:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        
        //systemName: "speaker.fill"
         //slide2.imageIcon.image = UIImage(named: "handEau")
        slide2.iconTop.image = UIImage(named: "2c")
        slide2.backImage.image = UIImage(named: "slide2")
        slide2.labelTitle.text = "Support"
        slide2.labelTitle.text = NSLocalizedString("Support", comment: "slide 2")
        slide2.labelDescription.text = "In this section, you can tap on the main button in order to send some help requests in case of potential danger."
        slide2.labelDescription.text = NSLocalizedString("SupportDesc", comment: "slide2")
        let slide3:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        //slide3.imageView.image = UIImage(named: "ic_onboarding_3")
        //slide3.imageIcon.image = UIImage(systemName: "text.bubble")
        slide3.iconTop.image = UIImage(named: "3c")
        slide3.backImage.image = UIImage(named: "slide3")
        slide3.labelTitle.text = "Requests"
        slide3.labelTitle.text = NSLocalizedString("Req", comment: "slide 3")
        slide3.labelDescription.text = "In this section, you will find a list of your help requests."
        slide3.labelDescription.text = NSLocalizedString("ReqDesc", comment: "slide3")
        let slide4:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        //slide4.imageView.image = UIImage(named: "ic_onboarding_4")
        //slide4.imageIcon.image = UIImage(systemName: "person.2")
        slide4.iconTop.image = UIImage(named: "4c")
        slide4.backImage.image = UIImage(named: "slide4")
        slide4.labelTitle.text = "Contacts"
        slide4.labelTitle.text = NSLocalizedString("Contact", comment: "slide 4")
        slide4.labelDescription.text = "In this section, you can select your favorite contacts in order to get help."
        slide4.labelDescription.text = NSLocalizedString("ContactDesc", comment: "slide 4")
        
        let slide5:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        //slide5.imageIcon.image = UIImage(named: "ic_onboarding_5")
        slide5.iconTop.image = UIImage(named: "5c")
        slide5.backImage.image = UIImage(named: "slide5")
        slide5.labelTitle.text = "Warning"
        slide5.labelDescription.text = "Use this app with caution, please."
        slide5.labelTitle.text = NSLocalizedString("WarnTitle", comment: "slide 5")
        slide5.labelDescription.text = NSLocalizedString("WarnDesc", comment: "slide 5")
       
        
        return [slide1, slide2, slide3, slide4, slide5]
    }
    func setupSlideScrollView(slides : [Slide]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.addSubview(slides[i])
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        if pageIndex == 4  && userDef == true{
            changeTitle()
            explore.isHidden = false
            explore.setTitle("Sign Up", for: .normal)
            explore.setTitle(NSLocalizedString("Register", comment: "Registrati"), for: .normal)
        }else{
            explore.isHidden = true
            if userDef == false{
            skipButton.setTitle("Skip", for: .normal)
                skipButton.setTitle(NSLocalizedString("Skip", comment: "Pulsante"), for: .normal)
            explore.isHidden = true
                
            }
        } //skipButton.setTitle("Start", for: .normal)
        if scrollView.contentOffset.y > 0 || scrollView.contentOffset.y < 0 {
            scrollView.contentOffset.y = 0
        }
        
    }
    
    func changeTitle() {
        skipButton.setTitle("Start", for: .normal)
        skipButton.setTitle(NSLocalizedString("Start", comment: "Pulsante"), for: .normal)
    }
    //MARK: nascondere status bar
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func skipButtonPressed(_ sender: Any) {
        if userDef {
            UserDefaults.standard.set(true, forKey: "tutorialAccepted")
            skipButton.isHidden = true
            //videoView.removeFromSuperview()
            ///self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let rootViewController = storyboard.instantiateViewController(withIdentifier: "SecondVC")
            AppDelegate.App.window?.rootViewController = rootViewController
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    
    @IBAction func whenExploreButtonIsPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let rootViewController = storyboard.instantiateViewController(withIdentifier: "loginViewController")
       
        self.present(rootViewController, animated: true, completion: nil)
        
        
        
        
        
    }
    
}


