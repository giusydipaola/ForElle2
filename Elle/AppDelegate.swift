//
//  AppDelegate.swift
//  Elle
//
//  Created by Gabriele Fioretti on 21/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit
import UserNotifications
import os.log
import WatchConnectivity
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    //MARK: - Soluzione: Usare una classe comune sia a watch che iOS
    let sharedInfo = SharedInfo.sharedInfo
    
    var window: UIWindow?
    var userData: UserDefaults
    var user: UserApi
    var helpRequestManager: HelpRequestManager
    //var session: WCSession!
    var currentStatus: RequestStatus = .finished
    var lastKnownWatchStatus: RequestStatus? = nil
    
    override init() {
        self.userData = sharedInfo.userData
        self.user = sharedInfo.user
        self.helpRequestManager = sharedInfo.helpRequestManager
        if !helpRequestManager.isActive {
            currentStatus = .finished
        } else {
            currentStatus = .started
        }
    }
    
    //public static var API_URL = "https://api.forelle.app/"
    
    public static var App: AppDelegate {
        get {
            return UIApplication.shared.delegate as! AppDelegate
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        if helpRequestManager.isActive {
            helpRequestManager.endRequest { success in
                
            }
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configureNotify(application, didFinishLaunchingWithOptions: launchOptions)
        
        
        print("Token: ", sharedInfo.tokenDevice)
        self.user.loadUser(callback: { (success, error) in
            print("logged \(self.user.isLogged)")
            if self.user.isLogged {
                self.updateTokenUser()
                self.helpRequestManager.loadCurrentRequest()
            }
            
        })
        UIApplication.shared.applicationIconBadgeNumber = 0
        return true
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("applicationDidEnterBackground")
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("applicationWillEnterForeground")
    }
    
}
// Gestione Notifiche
extension AppDelegate {
    func updateTokenUser() {
        if self.user.isLogged && sharedInfo.tokenDevice.count > 0 {
            self.user.updateTokenDevice(tokenDevice: sharedInfo.tokenDevice,callback: { (success, error) in
                if success {
                    print("Token device aggiornato")
                    
                } else {
                    print("Errore durante l'update del token")
                }
            })
        }
    }
    func broadcastNotify(userInfo: [AnyHashable: Any], showDetail: Bool) {
        print("broadcastNotify")
        print(userInfo)
        var category = ""
        var HelpRequest = 0
        
        if userInfo.keys.contains("HelpRequest") {
            if userInfo["HelpRequest"] is String {
                let HelpRequestTmp = userInfo["HelpRequest"] as! String
                HelpRequest = Int(HelpRequestTmp) ?? 0
            } else if userInfo["HelpRequest"] is Int {
                HelpRequest = userInfo["HelpRequest"] as! Int
            }
            
        }
        if userInfo.keys.contains("category") {
            category = userInfo["category"] as! String
        }
        let requestApi = HelpRequestApi()
        
        
        if HelpRequest > 0 {
            
            requestApi.find(id: HelpRequest ) { (success, error, requestData) in
                if(success) {
                    if category == HelpRequestCategory.HelpRequest.rawValue {
                        let name = Notification.Name("notifyHelpRequest")
                        NotificationCenter.default.post(name: name, object: requestData!)
                        if showDetail {
                            DispatchQueue.main.async {
                                let viewTmp = UIStoryboard(name: "Requests", bundle: nil).instantiateViewController(withIdentifier: "requestDetailView") as! RequestDetailViewController
                                viewTmp.helpRequest = requestData
                                if let root = self.window?.rootViewController {
                                    root.present(viewTmp, animated: true)
                                }
                            }
                        }
                    } else if category == HelpRequestCategory.UpdateRequest.rawValue {
                        //Cambio di logica nel backend: non dovrebbe essere più chiamata
                        let name = Notification.Name("notifyUpdateRequest")
                        NotificationCenter.default.post(name: name, object: requestData!)
                        
                    } else if category == HelpRequestCategory.EndRequest.rawValue {
                        let name = Notification.Name("notifyEndRequest")
                        NotificationCenter.default.post(name: name, object: requestData!)
                        if showDetail {
                            DispatchQueue.main.async {
                                let viewTmp = UIStoryboard(name: "Requests", bundle: nil).instantiateViewController(withIdentifier: "requestDetailView") as! RequestDetailViewController
                                viewTmp.helpRequest = requestData
                                if let root = self.window?.rootViewController {
                                    root.present(viewTmp, animated: true)
                                }
                            }
                        }
                    }
                } else {
                    print(error)
                }
            }
        }
    }
    
    func configureNotify(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        
        
        //Remove this method to stop OneSignal Debugging
        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)

        
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            
            //APERTURA AUTOMATICA
            print("Received Notification: \(String(describing: notification!.payload.notificationID))")
            /*if let notification = notification {
                if let additionalData = notification.payload.additionalData  {
                    self.broadcastNotify(userInfo: additionalData, showDetail: true)
                }
            }*/
        }
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            print("Received notificationOpenedBlock: \(String(describing: result!.notification.payload.notificationID))")
            
            let payload: OSNotificationPayload = result!.notification.payload
            
            let fullMessage = payload.body
            print("Message = \(String(describing: fullMessage))")
            //var newsId: String = ""
            
            
            if let additionalData = payload.additionalData  {
                self.broadcastNotify(userInfo: additionalData, showDetail: true)
            }
            
        }
        
        
         //START OneSignal initialization code
         let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false]
         
         // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
         OneSignal.initWithLaunchOptions(launchOptions,
                                            appId: "e5c63efd-878c-435f-91d1-113edc9fb6df",
                                            handleNotificationReceived: notificationReceivedBlock,
                                            handleNotificationAction: notificationOpenedBlock,
                                            settings: onesignalInitSettings)
         OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;

         // The promptForPushNotifications function code will show the iOS push notification prompt.
         //We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 6)
         OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
         })
         //END OneSignal initializataion code
         
         
    }
}

extension OSLog {
    private static var subsystem = Bundle.main.bundleIdentifier!
    
    /// Logs the view cycles like viewDidLoad.
    static let viewCycle = OSLog(subsystem: subsystem, category: "viewCycleElle")
}
