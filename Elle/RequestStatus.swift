//
//  Status.swift
//  Elle
//
//  Created by Gabriele Fioretti on 15/03/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import Foundation
/**
 * Per rappresentare lo stato di una richiesta
 */
public enum RequestStatus: String {
    case starting = "starting"
    case started = "started"
    case finishing = "finishing"
    case finished = "finished"
}
