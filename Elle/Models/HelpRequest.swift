//
//  HelpRequest.swift
//  Elle
//
//  Created by Fabio Palladino on 22/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import Foundation
enum HelpRequestCategory: String {
    case HelpRequest = "Help Request"
    case EndRequest = "End Request"
    case UpdateRequest = "Update Request"
}

class HelpRequestApi {
    var helpRequestData: HelpRequestDataResponse?
    
    init() {
    }
    func search(callback: @escaping((Bool,String,[HelpRequestDataResponse]?) -> Void)) {
        
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "api/help-request/")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error", nil)
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                let obj = try decoder.decode([HelpRequestDataResponse].self, from: jsonData)
                callback(true,"", obj)
            } catch let ex {
                print(ex)
                callback(false,"",nil)
            }
        })
        dataTask.resume()
    }
    func activeRequests(callback: @escaping((Bool,String,[HelpRequestDataResponse]?) -> Void)) {
        
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "api/help-request/active-requests")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error", nil)
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                let obj = try decoder.decode([HelpRequestDataResponse].self, from: jsonData)
                callback(true,"", obj)
            } catch let ex {
                print(ex)
                callback(false,"",nil)
            }
        })
        dataTask.resume()
    }
    func currentRequests(callback: @escaping((Bool,String,[HelpRequestDataResponse]?) -> Void)) {
        
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "api/help-request/current-requests")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error", nil)
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                let obj = try decoder.decode([HelpRequestDataResponse].self, from: jsonData)
                callback(true,"", obj)
            } catch let ex {
                print(ex)
                callback(false,"",nil)
            }
        })
        dataTask.resume()
    }
    func addRequest(helpRequest: HelpRequestAdd, callback: @escaping((Bool,String,HelpRequestDataResponse?) -> Void)) {
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "/api/help-request/add-request",method: "post")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let body = helpRequest.toJson()
        
        let dataTask = session.uploadTask(with: request, from: body.data(using: .utf8), completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error",nil)
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                self.helpRequestData = try decoder.decode(HelpRequestDataResponse.self, from: jsonData)
                callback(true,"",self.helpRequestData)
            } catch let ex {
                print(ex)
                callback(false,"", nil)
            }
            
        })
        dataTask.resume()
    }
    func updateRequest(helpRequestId: Int, severity: Int, active: Int, callback: @escaping((Bool,String,HelpRequestDataResponse?) -> Void)) {
        var updateRequest = HelpRequestUpdate()
        updateRequest.id = helpRequestId
        updateRequest.active = active
        updateRequest.serverity = severity
        
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "/api/help-request/update-request",method: "post")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let body = updateRequest.toJson()
        
        let dataTask = session.uploadTask(with: request, from: body.data(using: .utf8), completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error",nil)
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                self.helpRequestData = try decoder.decode(HelpRequestDataResponse.self, from: jsonData)
                callback(true,"",self.helpRequestData)
            } catch let ex {
                print(ex)
                callback(false,"", nil)
            }
            
        })
        dataTask.resume()
    }
    func addRequestDetail(detail: HelpRequestDetailAdd, callback: @escaping((Bool,String,HelpRequestDetailsResponse?) -> Void)) {
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "/api/help-request/add-request-details",method: "post")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let body = detail.toJson()
        
        let dataTask = session.uploadTask(with: request, from: body.data(using: .utf8), completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error",nil)
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                let requestDetail = try decoder.decode(HelpRequestDetailsResponse.self, from: jsonData)
                if self.helpRequestData != nil {
                    self.helpRequestData?.helpRequestDetails.append(requestDetail)
                }
                callback(true,"",requestDetail)
            } catch let ex {
                print(ex)
                callback(false,"", nil)
            }
            
        })
        dataTask.resume()
    }
    func find(id: Int, callback: @escaping((Bool,String,HelpRequestDataResponse?) -> Void)) {
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "api/help-request/get?id=\(id)")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error", nil)
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                let obj = try decoder.decode(HelpRequestDataResponse.self, from: jsonData)
                callback(true,"", obj)
            } catch let ex {
                print(ex)
                callback(false,"",nil)
            }
        })
        dataTask.resume()
    }
    func getFileAudio(id: Int, callback: @escaping((Bool,String,Data?) -> Void)) {
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "api/help-request/get-audio-file?id=\(id)")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let audioData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error", nil)
                return
            }
            callback(true,"", audioData)
        })
        dataTask.resume()
    }
    func deleteRequest(helpRequestId: Int, callback: @escaping((Bool,String) -> Void)) {
        
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "/api/help-request/delete?id=\(helpRequestId)",method: "delete")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let body = ""
        
        let dataTask = session.uploadTask(with: request, from: body.data(using: .utf8), completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error")
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                let dic = try decoder.decode([String:Bool].self, from: jsonData)
                if dic.keys.contains("success") {
                    callback(true,"")
                }
                else {
                    callback(false,"Error delete request")
                }
            } catch let ex {
                print(ex)
                callback(false,"")
            }
            
        })
        dataTask.resume()
    }
    func parameterMqtt(callback: @escaping((Bool,String,[String:String]?) -> Void)) {
        
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "api/help-request/parameter-mqtt")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error", nil)
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                let obj = try decoder.decode([String:String].self, from: jsonData)
                callback(true,"", obj)
            } catch let ex {
                print(ex)
                callback(false,"",nil)
            }
        })
        dataTask.resume()
    }
    func lastSeenFriends(id: Int, callback: @escaping((Bool,String,[LastSeenFriend]?) -> Void)) {
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "api/help-request/last-seen-friends?id=\(id)")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error", nil)
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                let obj = try decoder.decode([LastSeenFriend].self, from: jsonData)
                callback(true,"", obj)
            } catch let ex {
                print(ex)
                callback(false,"",nil)
            }
        })
        dataTask.resume()
    }
}

struct HelpRequestDetailAdd : Codable {
    var helpRequestId: Int
    var lat: Double
    var lon: Double
    var type: String
    var audioFile: String
    
    init() {
        self.helpRequestId = 0
        self.lon = 0
        self.lat = 0
        self.audioFile = ""
        self.type = "wav"
    }
    
    init(helpRequestId: Int, lat: Double, lon: Double, type: String = "wav", audioFileBase64: String) {
        self.helpRequestId = helpRequestId
        self.lon = lon
        self.lat = lat
        self.audioFile = audioFileBase64
        self.type = type
    }
    
    func toJson() -> String {
        let encode = JSONEncoder()
        var jsonValue: Data?
        do {
            encode.outputFormatting = .prettyPrinted
            jsonValue = try encode.encode(self)
        }
        catch let error {
            print("toJson -> \(error)")
        }
        if jsonValue != nil {
            let jsonString = String(data: jsonValue!, encoding: .utf8)
            return jsonString ?? ""
        }
        return ""
    }
}

struct HelpRequestUpdate: Codable {
    var id: Int
    var serverity: Int
    var active: Int
    
    init() {
        self.id = 0
        self.active = -1
        self.serverity = 0
    }
    
    func toJson() -> String {
        let encode = JSONEncoder()
        var jsonValue: Data?
        do {
            encode.outputFormatting = .prettyPrinted
            jsonValue = try encode.encode(self)
        }
        catch let error {
            print("toJson -> \(error)")
        }
        if jsonValue != nil {
            let jsonString = String(data: jsonValue!, encoding: .utf8)
            return jsonString ?? ""
        }
        return ""
    }
}

struct HelpRequestResponse: Codable {
    var id: Int
    var userId: Int
    var serverity: Int
    var lat: Double
    var lon: Double
    var description: String
    var dateInsert: String
    var dateModify: String
    var active: Int
    var publishQueue: String?
    
    init() {
        self.id = 0
        self.userId = 0
        self.serverity = 0
        self.lat = 0
        self.lon = 0
        self.description = ""
        self.dateInsert = ""
        self.dateModify = ""
        self.active = 0
        self.publishQueue = ""
    }
}

struct HelpRequestDetailsResponse: Codable {
    var id: Int
    var helpRequestId: Int
    var lat: Double
    var lon: Double
    var audioFileUrl: String
    var dateInsert: String
    var tokenKey: String?
    
    init() {
        self.id = 0
        self.helpRequestId = 0
        self.lat = 0.0
        self.lon = 0.0
        self.audioFileUrl = ""
        self.dateInsert = ""
        self.tokenKey = ""
    }
    func getPublicUrl() -> String {
        return  "\(SharedInfo.API_URL)api/help-request/download?key=\(self.tokenKey!)"
    }
}

struct HelpRequestNotificationsResponse: Codable {
    var id: Int
    var userId: Int
    var helpRequestId: Int
    var friendId: Int
    var dateInsert: String
    var dateLastSeen: String?
    
    init() {
        self.id = 0
        self.userId = 0
        self.helpRequestId = 0
        self.friendId = 0
        self.dateInsert = ""
        self.dateLastSeen = ""
    }
}

struct HelpRequestDataResponse: Codable {
    var helpRequest: HelpRequestResponse
    var helpRequestDetails: [HelpRequestDetailsResponse]
    var helpRequestNotifications: [HelpRequestNotificationsResponse]
    var user: UserResponse?
    
    init() {
        self.helpRequest = HelpRequestResponse()
        self.helpRequestDetails = [HelpRequestDetailsResponse]()
        self.helpRequestNotifications = [HelpRequestNotificationsResponse]()
    }
}
struct HelpRequestAdd : Codable {
    var serverity: Int
    var lat: Double
    var lon: Double
    var description: String
    
    init(severity: Int = 0, lat: Double = 0.0, lon: Double = 0.0, description: String = "") {
        self.serverity = severity
        self.lat = lat
        self.lon = lon
        self.description = description
    }
    
    func toJson() -> String {
        let encode = JSONEncoder()
        var jsonValue: Data?
        do {
            encode.outputFormatting = .prettyPrinted
            jsonValue = try encode.encode(self)
        }
        catch let error {
            print("toJson -> \(error)")
        }
        if jsonValue != nil {
            let jsonString = String(data: jsonValue!, encoding: .utf8)
            return jsonString ?? ""
        }
        return ""
    }
}
struct LastSeenFriend : Codable {
    var friendId: Int
    var name: String
    var dateLastSeen: String
        
    init() {
        self.friendId = 0
        self.name = ""
        self.dateLastSeen = ""
    }
}
struct MqttNotify : Codable {
    var type: String
    var timeStamp: String
    var data: String
    
    init() {
        self.type = ""
        self.data = ""
        self.timeStamp = ""
    }
    var isDetail: Bool {
        get {
            if type == "detail" {
                return true
            }
            return false
        }
    }
    var isLastSeenFriends: Bool {
        get {
            if type == "last-seen" {
                return true
            }
            return false
        }
    }
    var isEnd: Bool {
        get {
            if type == "end" {
                return true
            }
            return false
        }
    }
    func toDetail() -> HelpRequestDetailsResponse? {
        if self.data.count == 0 {
            return nil
        }
        let decoder = JSONDecoder()
        do {
            let obj = try decoder.decode(HelpRequestDetailsResponse.self, from: self.data.data(using: .utf8)!)
            return obj
        } catch let ex {
            print(ex)
        }
        return nil
    }
    func toLastSeenFriends() -> [LastSeenFriend]? {
        if self.data.count == 0 {
            return nil
        }
        let decoder = JSONDecoder()
        do {
            let obj = try decoder.decode([LastSeenFriend].self, from: self.data.data(using: .utf8)!)
            return obj
        } catch let ex {
            print(ex)
        }
        return nil
    }
    static func parseObject(message: String) -> MqttNotify? {
        let decoder = JSONDecoder()
        do {
            let obj = try decoder.decode(MqttNotify.self, from: message.data(using: .utf8)!)
            return obj
        } catch let ex {
            print(ex)
        }
        return nil
    }
}
