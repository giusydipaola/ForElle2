//
//  TableViewCell.swift
//  Elle
//
//  Created by Giusy Di Paola on 25/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var tutorialLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
