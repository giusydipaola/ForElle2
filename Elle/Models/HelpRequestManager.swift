//
//  HelpRequestManager.swift
//  Elle
//
//  Created by Fabio Palladino on 22/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import Foundation
import WatchConnectivity
import CoreLocation
import AVFoundation

class HelpRequestManager: NSObject {
    var date: Date!
    var locationManager = CLLocationManager()
    var recordingTimer: Timer?
    let audioFragmentDuration = 30.0
    var location: CLLocation!                           //Last known location
    var positionAddress: String = ""                    //String that represents last position of user
    var avRecorder: AVAudioRecorder!                    //To record audio
    var avSession: AVAudioSession!                      //Session for record audio
    var helpRequest: HelpRequestDataResponse? = nil     //Initialized to nil because at the beginning there is no helpRequest
    let SECOND_TIMER = 10.0
    var timerCount = 0
    var currentUser: UserCreate?
//    var session: WCSession? = nil
//    var hasToStop: Bool = false
    
    
    //Settings to audio recorder. To save in m4a in high quality
    let recordSettings = [
        AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
        AVSampleRateKey: 12000,
        AVNumberOfChannelsKey: 1,
        AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
    ]
    
    override init() {
        super.init()
        self.initializeLocationManager()
//        self.configureWatchSession()
//        if session != nil {
//            session?.transferUserInfo(["status": self.isActive])
//            print("status transfered")
//        }
    }
    
    var isActive: Bool {
        if(helpRequest != nil) {                            //MARK:Perché doppio controllo?
            if(helpRequest?.helpRequest.active == 1) /* && !hasToStop */{
                return true
            }
        }
        return false
    }
    
    func startTimer() {
        if recordingTimer != nil {
            recordingTimer?.invalidate()
            recordingTimer = nil
        }
        recordingTimer =  Timer.scheduledTimer(timeInterval: SECOND_TIMER,
                                               target: self,
                                               selector: #selector(mainTimer),
                                               userInfo: nil,
                                               repeats: true)
    }
    
    @objc func mainTimer() {
        let now = Date()
        timerCount += 1
        print("mainTimer \(now)")
        // Ogni 10 secondi invia un dettaglio della posizione, ogni 30 audio registrato
        //Aggiorno cos' la posizione: inviando un detail con stringa audio vuota
        if (timerCount > 2) {      //Passano 30 secondi. > 2 è per sicurezza
            timerCount = 0          //Resettiamo lo stato del timer
            finishRecording(success: true, isLastUpdate: false) { success in
            }
            startRecording { (success) in
            }
        } else {
            addRequestDetail(audiob64Str: "") { (success, errorMsg, helpReqDataResp) in
                if success {
                    if let response = helpReqDataResp {
                        self.helpRequest = response
                    } else {
                        print(errorMsg)
                    }
                } else {
                    print(errorMsg)
                }
            }
        }
    }
    
    //MARK: - Functions for recording
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    //MARK: - Functions fermare ed inizializzare il timer e l'oggetto recording
    func clearRecordingObject() {
        if recordingTimer != nil {
            //Stopping the timer
            recordingTimer?.invalidate()
            recordingTimer = nil
        }
        if self.avRecorder != nil {
            if self.avRecorder.isRecording {
                self.avRecorder.stop()
            }
            self.avRecorder = nil
        }
    }
    func startRecording(callback: @escaping((Bool) -> Void)) {
//        configureWatchSession()
        self.initializeLocationManager()
        if !self.isActive {
            self.clearRecordingObject()
            addRequest { (success, errorMsg, helpReqDataResp) in
                if !success {
                    print(errorMsg)
                    callback(false)
                } else {
//                    if self.session != nil {
//                        self.session!.transferUserInfo(["status": self.isActive == true ? true : false]) //Manda lo stato della richiesta al watch
//                        print("State transfered")
//                    } else {
//                        print("No active session")
//                    }
                    print("Request successfully sent!")
                    callback(true)
                    //Salvataggio locale dell'audio
                    let audioFilename = self.getDocumentsDirectory().appendingPathComponent("recording.m4a")
                    do {
                        self.avRecorder = try AVAudioRecorder(url: audioFilename, settings: self.recordSettings)
                        self.avRecorder.delegate = self
                        self.avRecorder.record()
                    } catch {
                        self.finishRecording(success: false, isLastUpdate: false) { success in
                            callback(false)
                        }
                    }
                }
            }
        }
        else if self.avRecorder == nil
        {
            //Nel caso di successivo invio viene reinizzializzato il componente
            let audioFilename = self.getDocumentsDirectory().appendingPathComponent("recording.m4a")
            do {
                self.avRecorder = try AVAudioRecorder(url: audioFilename, settings: self.recordSettings)
                self.avRecorder.delegate = self
                self.avRecorder.record()
            } catch {
                self.finishRecording(success: false, isLastUpdate: false) { success in
                    callback(false)
                }
            }
        }
    }
    
    /**
     *  Questa funzione viene chiamata quando si stoppa la richiesta d'aiuto.
     *  Il suo compito è di:
     *  1) Interrompere la localizzazione
     *  2) Interrompere il timer
     */
    func endRequest(callback: @escaping((Bool) -> Void)) {
        //configureWatchSession()
        self.locationManager.stopUpdatingLocation()
        self.finishRecording(success: true, isLastUpdate: true) { success in
            if success {
//                if self.session != nil {
//                    self.session!.transferUserInfo(["status": self.isActive == true ? true : false]) //Manda lo stato della richiesta al watch
//                    print("State transfered")
//                } else {
//                    print("No active session")
//                }
            }
            self.recordingTimer?.invalidate()
            self.recordingTimer = nil
            self.helpRequest?.helpRequest.active = 0
//            self.hasToStop = true
            callback(success)
        }
        
    }
    /**
     * Funzione che viene chiamata ogni volta che finisce di registrare un audio fragment.
     * - Parameter success: Indica se l'audio è stato registrato con successo o se la registrazione si è fermata per errore
     * - Parameter isLastUpdate: True se questo finishRecording viene chiamato dal pulsante Stop
     */
    func finishRecording(success: Bool, isLastUpdate: Bool, callback: @escaping((Bool) -> Void)) {
        //MARK: Localizzazione mai interrotta fino allo stop, vero?
        //        let audioLenght = self.avRecorder.currentTime
        self.avRecorder?.stop()
        
        if !success {
            self.avRecorder = nil
            return
        }
        var audioData: Data?
        if avRecorder != nil {
            do {
                audioData = try Data(contentsOf: avRecorder.url)
            } catch let error as NSError {
                print("\(error)\n\(error.description)")
            }
        }
        
        if audioData != nil {
            let audiob64Str = audioData?.base64EncodedString()
            self.addRequestDetail(audiob64Str: audiob64Str ?? "") { (success, errorMsg, helpRequestDataResponse) in
                if !success {
                    print(errorMsg)
                }
                if isLastUpdate {
                    self.stopRequest { (success, errorMsg, helpReqDataResp) in
                        if !success {
                            print(errorMsg)
                        }
                        callback(success)
                    }
                }
            }
        } else {
            print("Error in getting audio data")
            if isLastUpdate {
                self.stopRequest { (success, errorMsg, helpReqDataResp) in
                    if !success {
                        print(errorMsg)
                    }
                    callback(success)
                }
            }
        }
        print("Recording added") //MARK: Debug print
        self.avRecorder = nil
    }
    
    func loadCurrentRequest() {
        let api = HelpRequestApi()
        api.currentRequests { (success, error, datiRequests) in
            if success {
                if let datiRequests = datiRequests {
                    if datiRequests.count > 0 {
                        self.stopRequestPending(requests: datiRequests)
                    }
                }
            } else {
                print("ERRORE loadCurrentRequest \(error)")
            }
        }
    }
    func stopRequestPending(requests: [HelpRequestDataResponse]) {
        let api = HelpRequestApi()
        for r in requests {
            api.updateRequest(helpRequestId: r.helpRequest.id, severity: r.helpRequest.serverity, active: 0) { (success, error, datiRequest) in
                if(success) {
                    print("stopRequestPending  \(r.helpRequest.id)")
                }
            }
        }
    }
    /**
     * Function used to start a new HelpRequest. Called in StartRecording
     */
    func addRequest(callback: @escaping((Bool,String,HelpRequestDataResponse?) -> Void)) {
        if isActive {
            return                      //MARK: Per evitare di aggiungere una richiesta se ce ne sta già una in corso?
        }
        let app = SharedInfo.sharedInfo
        let api = HelpRequestApi()
        var request = HelpRequestAdd()
        guard let pos = self.location else {
            callback(false, "Invalid location", nil)
            return
        }
        let location = pos.coordinate
        //Building request object
        request.serverity = 1
        //request.description = "Help Request from \(app.user.user.name)"
        request.description = String(format: NSLocalizedString("Support_Request", comment: ""), "\(app.user.user.name)")
            
           
        request.lat = location.latitude
        request.lon = location.longitude
        //Starting timer
        startTimer()
        //Send request to servers
        api.addRequest(helpRequest: request) { (success, error, datiRequest) in
            if(success) {
                //                self.hasToStop = false
                self.helpRequest = datiRequest
                DispatchQueue.main.async {
                    callback(true, "", self.helpRequest)
                    self.startTimer()
                }
            } else {
                callback(false,error,nil)
            }
        }
    }
    /**
     * This function is called to upload audio and position
     */
    func addRequestDetail(audiob64Str: String, callback: @escaping(Bool,String,HelpRequestDataResponse?) -> Void) {
        let api = HelpRequestApi()
        if isActive {
            if let helpRequest = helpRequest?.helpRequest {
                let reqDetailAdd = HelpRequestDetailAdd(helpRequestId: helpRequest.id, lat: location.coordinate.latitude, lon: location.coordinate.longitude, type: "m4a", audioFileBase64: audiob64Str)
                api.addRequestDetail(detail: reqDetailAdd) { (success, errorMsg, helpReqDetailResp) in
                    if success {
                        if let helpReqDetailResp = helpReqDetailResp {
                            self.helpRequest?.helpRequestDetails.append(helpReqDetailResp)  //MARK: TODO check
                            callback(true, "", self.helpRequest)
                        }
                        else {
                            callback(false, "Invalid response", nil)
                        }
                    } else {
                        callback(false, "Unable to update request", nil)
                    }
                }
            }
        }
    }
    /**
     * This function is called whrn the stop button is pressed
     */
    func stopRequest(callback: @escaping((Bool,String,HelpRequestDataResponse?) -> Void)) {
        let api = HelpRequestApi()
        if recordingTimer != nil {
            //Stopping the timer
            recordingTimer?.invalidate()
            recordingTimer = nil
        }
        if isActive {
            if let helpRequest = helpRequest?.helpRequest {
                api.updateRequest(helpRequestId: helpRequest.id, severity: helpRequest.serverity, active: 0) { (success, error, datiRequest) in
                    if(success) {
                        self.helpRequest = datiRequest
                    }
                    callback(success,error,self.helpRequest)
                }
                self.helpRequest = nil                  //Metto a nil così da poter avviare la nuova richiesta
            } else {
                DispatchQueue.main.async {
                    callback(false,"Request nil", nil)
                }
            }
        } else {
            DispatchQueue.main.async {
                callback(false,"Request Not Active", nil)
            }
        }
    }
    
    /**
     * This function is called when the user taps the button when the request is already started (to update severity)
     */
    func updateSeverity(callback: @escaping((Bool,String,HelpRequestDataResponse?) -> Void)) {
        let api = HelpRequestApi()
        if isActive {
            if let helpRequest = helpRequest?.helpRequest {
                api.updateRequest(helpRequestId: helpRequest.id, severity: helpRequest.serverity+1, active: 1) { (success, error, datiRequest) in
                    if(success) {
                        self.helpRequest = datiRequest
                    }
                    callback(success,error,self.helpRequest)
                }
            } else {
                DispatchQueue.main.async {
                    callback(false,"Request nil", nil)
                }
            }
        } else {
            DispatchQueue.main.async {
                callback(false,"Request Not Active", nil)
            }
        }
    }
    
    //MARK: - Configure watchSession
//    func configureWatchSession() {
//        if session == nil {
//            if WCSession.isSupported() {
//                session = WCSession.default
//                if session!.isPaired && session!.isWatchAppInstalled {
//                    session?.delegate = self
//                    session?.activate()
//                } else {session = nil}
//            }
//        }
//    }
    
    //MARK: - Initialization of Location Manager
    func initializeLocationManager() {
        // For use in foreground
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                break
            case .authorizedAlways, .authorizedWhenInUse:
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            @unknown default:
                break
            }
        } else {
            return
        }
    }
}

//MARK: - Extension for AVFoundation
extension HelpRequestManager: AVAudioRecorderDelegate {
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            self.avRecorder = nil
        }
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Error while recording audio \(error!.localizedDescription)")
    }
}

//MARK: - Extension Location Manager
extension HelpRequestManager: CLLocationManagerDelegate {
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.location = locations[0]
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(self.location) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode \(String(describing: error))")
            }
            guard let placemarks = placemarks else {return}
            let placemark = placemarks as [CLPlacemark]
            if placemark.count>0{
                let placemark = placemarks[0]
                if (placemark.locality != nil) {
                    if let locality = placemark.locality {
                        if let sublocality = placemark.subLocality {
                            self.positionAddress = "\(locality), \(sublocality)"
                        } else {
                            self.positionAddress = "\(locality)"
                        }
                    } else {
                        self.positionAddress = "Unknown"
                    }
                }
            }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: \(error)")
    }
}

////MARK: Extension for watchConnectivity
//extension HelpRequestManager: WCSessionDelegate {
//
//    func sessionDidBecomeInactive(_ session: WCSession) {
//    }
//
//    func sessionDidDeactivate(_ session: WCSession) {
//    }
//
//    func sessionReachabilityDidChange(_ session: WCSession) {
//        if session.isPaired && session.isReachable {
//            if session.activationState == .activated {
//                session.transferUserInfo(["status": self.isActive == true ? true : false]) //Manda lo stato della richiesta al watch
//                print("State transfered")
//            }
//        }
//    }
//
//    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
//        switch activationState {
//        case .activated:
//            session.transferUserInfo(["status": self.isActive == true ? true : false]) //Manda lo stato della richiesta al watch
//            print("State transfered")
//            break
//        case .inactive:
//            break
//        case .notActivated:
//            break
//        @unknown default:
//            break
//        }
//    }
//
//    //To do something with recieved datas
//    func session(_ session: WCSession, didReceiveUserInfo userInfo: [String : Any] = [:]) {
//        let recordingStarted = userInfo["recordingStarted"] as! Bool
//        if recordingStarted{
//            if !self.isActive{
//                self.startRecording { (success) in
////                    if success {
////                        session.transferUserInfo(["success": true])
////                    } else {
////                        session.transferUserInfo(["success": false])
////                    }
//                }
//            }
//        } else {
//            if self.isActive{
//                self.endRequest { (success) in
////                    if success {
////                        session.transferUserInfo(["success": true])
////                    } else {
////                        session.transferUserInfo(["success": false])
////                    }
//                }
//            }
//        }
//    }
//}
