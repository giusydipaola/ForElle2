//
//  UtilsExtension.swift
//  Elle
//
//  Created by Gabriele Fioretti on 03/03/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import Foundation
import UIKit

extension Utils {
    public static func ShowMessage(vc: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
}
