//
//  PulseAnimation.swift
//  Elle
//
//  Created by Giusy Di Paola on 24/02/20.
//  Copyright © 2020 Giusy Di Paola. All rights reserved.
//

import UIKit

class PulseAnimation: CALayer {

    var animationGroup = CAAnimationGroup()
    var animationDuration: TimeInterval = 1.5
    var radius: CGFloat = 200
    var numebrOfPulse: Float = Float.infinity
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(numberOfPulse: Float = Float.infinity, radius: CGFloat, postion: CGPoint){
        super.init()
        self.backgroundColor = UIColor.black.cgColor
        self.contentsScale = UIScreen.main.scale
        self.opacity = 0
        self.radius = radius
        self.numebrOfPulse = numberOfPulse
        self.position = postion
        
        self.bounds = CGRect(x: 0, y: 0, width: radius*2, height: radius*2)
        self.cornerRadius = radius
        
        DispatchQueue.global(qos: .default).async {
            self.setupAnimationGroup()
            DispatchQueue.main.async {
                self.add(self.animationGroup, forKey: "pulse")
           }
        }
    }
    
    func scaleAnimation() -> CABasicAnimation {
        let scaleAnimaton = CABasicAnimation(keyPath: "transform.scale.xy")
        scaleAnimaton.fromValue = NSNumber(value: 0)
        scaleAnimaton.toValue = NSNumber(value: 1)
        scaleAnimaton.duration = animationDuration
        return scaleAnimaton
    }
    
    func createOpacityAnimation() -> CAKeyframeAnimation {
        let opacityAnimiation = CAKeyframeAnimation(keyPath: "opacity")
        opacityAnimiation.duration = animationDuration
        opacityAnimiation.values = [0.4,0.8,0]
        opacityAnimiation.keyTimes = [0,0.3,1]
        return opacityAnimiation
    }
    
    func setupAnimationGroup() {
        self.animationGroup.duration = animationDuration
        self.animationGroup.repeatCount = numebrOfPulse
        let defaultCurve = CAMediaTimingFunction(name: CAMediaTimingFunctionName.default)
        self.animationGroup.timingFunction = defaultCurve
        self.animationGroup.animations = [scaleAnimation(),createOpacityAnimation()]
    }
    
    
}

// PULSING BUTTON------------------------------------------------------
   /*func createPulse() {
    
    for _ in 0...2 {
    let circularPath = UIBezierPath(arcCenter: .zero, radius: UIScreen.main.bounds.size.width/2.0, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
    let pulseLayer = CAShapeLayer()
    pulseLayer.path = circularPath.cgPath
    pulseLayer.lineWidth = 2.0
    pulseLayer.fillColor = UIColor.clear.cgColor
    pulseLayer.lineCap = CAShapeLayerLineCap.round
    pulseLayer.position = CGPoint(x: mainButton.frame.size.width/2.0, y: mainButton.frame.size.width/2.0)
    mainButton.layer.addSublayer(pulseLayer)
    pulseLayers.append(pulseLayer)
    }
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
    self.animatePulse(index: 0)
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
    self.animatePulse(index: 1)
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
    self.animatePulse(index: 2)
    }
    }
    }
    }
    
    func animatePulse(index: Int) {
    pulseLayers[index].strokeColor = myColor.cgColor
    let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
    scaleAnimation.duration = 2.0
    scaleAnimation.fromValue = 0.2
    scaleAnimation.toValue = 0.9
    scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
    scaleAnimation.repeatCount = .greatestFiniteMagnitude
    pulseLayers[index].add(scaleAnimation, forKey: "scale")
    
    let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
    opacityAnimation.duration = 2.0
    opacityAnimation.fromValue = 0.9
    opacityAnimation.toValue = 0.0
    opacityAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
    opacityAnimation.repeatCount = .greatestFiniteMagnitude
    pulseLayers[index].add(opacityAnimation, forKey: "opacity")
    }*/
   //------End-------------------------------------------------------
   
