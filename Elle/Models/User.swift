//
//  User.swift
//  Elle
//
//  Created by Fabio Palladino on 21/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import Foundation
class UserApi: Codable {
    var token: String
    var user: UserResponse
    var friends: UserFriendsResponse
    
    init() {
        self.user = UserResponse()
        self.friends = UserFriendsResponse()
        
        let defaults = UserDefaults.standard
        self.token = defaults.string(forKey: "token") ?? ""
        print("Token auth: \(self.token)")
    }
    var isLogged: Bool {
        get {
            if token.count > 0 {
                return true
            }
            return false
        }
    }
    func saveValue(token: String) {
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: "token")
        self.token = token
    }
    func logout() {
        saveValue(token: "")
        self.user = UserResponse()
        self.friends = UserFriendsResponse()
    }
    func createUrlRequest(urlAction: String, method: String = "get") -> URLRequest {
        let urlString = "\(SharedInfo.API_URL)\(urlAction)"
        
        let safeUrl = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: safeUrl!)
        var request = URLRequest(url: url!)
        request.setValue("Bearer \(self.token)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = method
        return request
    }
    
    func loadUser(callback: @escaping((Bool,String) -> Void)) {
        if !self.isLogged {
            callback(false,"Not Logged")
            return;
        }
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "api/user/get")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error")
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                self.user = try decoder.decode(UserResponse.self, from: jsonData)
                callback(true,"")
                self.loadFriends(callback: { (success, error) in
                    if(!success) {
                        print("loadFriends \(error)")
                    }
                })
            } catch let ex {
                print(ex)
                callback(false,ex.localizedDescription)
            }
            
        })
        dataTask.resume()
    }
    func loadFriends(callback: @escaping((Bool,String) -> Void)) {
        if !self.isLogged {
            callback(false,"Not Logged")
            return;
        }
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "api/user/friends")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error")
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                self.friends = try decoder.decode(UserFriendsResponse.self, from: jsonData)
                callback(true,"")
            } catch let ex {
                print(ex)
                callback(false,ex.localizedDescription)
            }
            
        })
        dataTask.resume()
    }
    func updateUser(name: String, number: String, callback: @escaping((Bool,String,UserResponse?) -> Void)) {
        if !self.isLogged {
            callback(false,"Not Logged",nil)
            return;
        }
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "/api/user/update",method: "post")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        var user = UserUpdate()
        user.name = name
        user.number = number
        let body = user.toJson()
        
        let dataTask = session.uploadTask(with: request, from: body.data(using: .utf8), completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error", nil)
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                self.user = try decoder.decode(UserResponse.self, from: jsonData)
                callback(true,"",self.user)
            } catch let ex {
                print(ex)
                callback(false,ex.localizedDescription,nil)
            }
            
        })
        dataTask.resume()
    }
    func addFriend(friendId: [Int], callback: @escaping((Bool,String,[UserResponse]?) -> Void)) {
        if !self.isLogged {
            callback(false,"Not Logged",nil)
            return;
        }
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "/api/user/add-friend",method: "post")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        var friendAdd = UserFriendsAdd()
        friendAdd.friendId = friendId
        let body = friendAdd.toJson()
        
        let dataTask = session.uploadTask(with: request, from: body.data(using: .utf8), completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error", nil)
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                var users = [UserResponse]()
                let friend = try decoder.decode([UserFriendsAddReponse].self, from: jsonData)
                for f in friend {
                    users.append(f.friend)
                }
                callback(true,"",users)
            } catch let ex {
                print(ex)
                callback(false,ex.localizedDescription,nil)
            }
            
        })
        dataTask.resume()
    }
    func deleteFriend(friendId: Int, callback: @escaping((Bool, String) -> Void)) {
        if !self.isLogged {
            callback(false,"")
            return
        }
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "/api/user/delete-friend",method: "post")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        var friendAdd = UserFriendsDelete()
        friendAdd.friendId = friendId
        let body = friendAdd.toJson()
        
        let dataTask = session.uploadTask(with: request, from: body.data(using: .utf8), completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error")
                return
            }
            //let output = String(data: jsonData, encoding: .utf8)
            let decoder = JSONDecoder()
            do {
                let friend = try decoder.decode(UserFriendsAddReponse.self, from: jsonData)
                if friend.success {
                    callback(true,"Error delete")
                }
                else {
                    callback(false,"Error create user")
                }
            } catch let ex {
                print(ex)
                callback(false,ex.localizedDescription)
            }
            
        })
        dataTask.resume()
    }
    func updateTokenDevice(tokenDevice: String, callback: @escaping((Bool,String) -> Void)) {
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "/api/user/update-token",method: "post")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        var dic = [String:String]()
        dic["tokenDevice"] = tokenDevice
        let body = Utils.dicToJson(dic: dic)
        
        let dataTask = session.uploadTask(with: request, from: body.data(using: .utf8), completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error")
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                let dic = try decoder.decode([String:Bool].self, from: jsonData)
                if dic.keys.contains("success") {
                    callback(true,"")
                }
                else {
                    callback(false,"Error create user")
                }
            } catch let ex {
                print(ex)
                callback(false,"")
            }
            
        })
        dataTask.resume()
    }
    func createUser(user: UserCreate, callback: @escaping((Bool,String) -> Void)) {
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "/api/user/create",method: "post")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let body = user.toJson()
        
        let dataTask = session.uploadTask(with: request, from: body.data(using: .utf8), completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error")
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                let dic = try decoder.decode([String:String].self, from: jsonData)
                if dic.keys.contains("token") {
                    self.saveValue(token: dic["token"] ?? "")
                    if self.isLogged {
                        self.loadUser { (success, error) in
                            callback(self.isLogged,"")
                        }
                    } else {
                        callback(self.isLogged,"User not logged")
                    }
                }
                else {
                    callback(false,"Error create user")
                }
            } catch let ex {
                print(ex)
                callback(false,"")
            }
            
        })
        dataTask.resume()
    }
    func login(username: String, password: String, callback: @escaping((Bool,String) -> Void)) {
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "/api/auth/login",method: "post")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        var login = LoginRequest()
        login.username = username
        login.password = password
        
        let body = login.toJson()
        
        let dataTask = session.uploadTask(with: request, from: body.data(using: .utf8), completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error")
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                let dic = try decoder.decode([String:String].self, from: jsonData)
                if dic.keys.contains("token") {
                    self.saveValue(token: dic["token"] ?? "")
                    if self.isLogged {
                        self.loadUser { (success, error) in
                            callback(self.isLogged,"")
                        }
                    } else {
                        callback(self.isLogged,"User not logged")
                    }
                }
                else {
                    callback(false,"Error create user")
                }
            } catch let ex {
                print(ex)
                callback(false,"")
            }
            
        })
        dataTask.resume()
    }
    func findFriends(friends: [String],  callback: @escaping((Bool,String, [UserResponse]?) -> Void)) {
        if !self.isLogged {
            callback(false,"Not Logged",nil)
            return;
        }
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "api/user/find-friends", method: "post")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let encode = JSONEncoder()
        var jsonValue: Data?
        var body = ""
        do {
            encode.outputFormatting = .prettyPrinted
            jsonValue = try encode.encode(friends)
        }
        catch let error {
            print("toJson -> \(error)")
        }
        if jsonValue != nil {
            body = String(data: jsonValue!, encoding: .utf8) ?? ""
        }
        
        let dataTask = session.uploadTask(with: request, from: body.data(using: .utf8), completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error",nil)
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                let friendsFound = try decoder.decode([UserResponse].self, from: jsonData)
                callback(true,"",friendsFound)
            } catch let ex {
                print(ex)
                callback(false,"",nil)
            }
            
        })
        dataTask.resume()
    }
    func findMyFriends(callback: @escaping((Bool,String, [UserResponse]?) -> Void)) {
        if !self.isLogged {
            callback(false,"Not Logged",nil)
            return;
        }
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "api/user/find-my-friends", method: "get")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error",nil)
                return
            }
            let output = String(data: jsonData, encoding: .utf8)
            print(output!)
            let decoder = JSONDecoder()
            do {
                let friendsFound = try decoder.decode([UserResponse].self, from: jsonData)
                callback(true,"",friendsFound)
            } catch let ex {
                print(ex)
                callback(false,"",nil)
            }
            
        })
        dataTask.resume()
    }
    func deleteMyFriend(friendId: Int, callback: @escaping((Bool, String, [UserResponse]?) -> Void)) {
        if !self.isLogged {
            callback(false,"", nil)
            return
        }
        let app = SharedInfo.sharedInfo
        let request = app.user.createUrlRequest(urlAction: "/api/user/delete-my-friend",method: "delete")
        let session = URLSession(configuration: .default,delegate: nil,delegateQueue: .main)
        
        var friendAdd = UserFriendsDelete()
        friendAdd.friendId = friendId
        let body = friendAdd.toJson()
        
        let dataTask = session.uploadTask(with: request, from: body.data(using: .utf8), completionHandler: { (data, response, error) in
            guard let jsonData = data else {
                print("data is not valid")
                print(error?.localizedDescription ?? "no error")
                callback(false,error?.localizedDescription ?? "no error", nil)
                return
            }
            //let output = String(data: jsonData, encoding: .utf8)
            let decoder = JSONDecoder()
            do {
                let friend = try decoder.decode(UserFriendDeleteReponse.self, from: jsonData)
                if friend.success {
                    callback(true,"",friend.friend)
                }
                else {
                    callback(false,"Error create user", nil)
                }
            } catch let ex {
                print(ex)
                callback(false,ex.localizedDescription,nil)
            }
            
        })
        dataTask.resume()
    }
}
struct LoginRequest: Codable {
    var username: String
    var password: String
    
    init() {
        self.username = ""
        self.password = ""
    }
    func toJson() -> String {
        let encode = JSONEncoder()
        var jsonValue: Data?
        do {
            encode.outputFormatting = .prettyPrinted
            jsonValue = try encode.encode(self)
        }
        catch let error {
            print("toJson -> \(error)")
        }
        if jsonValue != nil {
            let jsonString = String(data: jsonValue!, encoding: .utf8)
            return jsonString ?? ""
        }
        return ""
    }
}
struct UserCreate: Codable {
    var id: Int
    var username: String
    var password: String
    var name: String
    var number: String
    var tokenDevice: String
    
    init() {
        self.id = 0
        self.username = ""
        self.password = ""
        self.name = ""
        self.number = ""
        self.tokenDevice = ""
    }
    func toJson() -> String {
        let encode = JSONEncoder()
        var jsonValue: Data?
        do {
            encode.outputFormatting = .prettyPrinted
            jsonValue = try encode.encode(self)
        }
        catch let error {
            print("toJson -> \(error)")
        }
        if jsonValue != nil {
            let jsonString = String(data: jsonValue!, encoding: .utf8)
            return jsonString ?? ""
        }
        return ""
    }
}
struct UserUpdate: Codable {
    var name: String
    var number: String
    init() {
        self.name = ""
        self.number = ""
    }
    func toJson() -> String {
        let encode = JSONEncoder()
        var jsonValue: Data?
        do {
            encode.outputFormatting = .prettyPrinted
            jsonValue = try encode.encode(self)
        }
        catch let error {
            print("toJson -> \(error)")
        }
        if jsonValue != nil {
            let jsonString = String(data: jsonValue!, encoding: .utf8)
            return jsonString ?? ""
        }
        return ""
    }
}
struct UserResponse: Codable {
    var id: Int
    var name: String
    var number: String
    var tokenDevice: String
    
    init() {
        self.id = 0
        self.name = ""
        self.number = ""
        self.tokenDevice = ""
    }
}
struct UserFriendsResponse: Codable {
    var userId: Int
    var friends: [UserResponse]
    
    init() {
        self.userId = 0
        self.friends = [UserResponse]()
    }
}
struct UserFriendsAdd: Codable {
    var friendId: [Int]
    
    init() {
        self.friendId = [Int]()
    }
    func toJson() -> String {
        let encode = JSONEncoder()
        var jsonValue: Data?
        do {
            encode.outputFormatting = .prettyPrinted
            jsonValue = try encode.encode(self)
        }
        catch let error {
            print("toJson -> \(error)")
        }
        if jsonValue != nil {
            let jsonString = String(data: jsonValue!, encoding: .utf8)
            return jsonString ?? ""
        }
        return ""
    }
}
struct UserFriendsDelete: Codable {
    var friendId: Int
    
    init() {
        self.friendId = 0
    }
    func toJson() -> String {
        let encode = JSONEncoder()
        var jsonValue: Data?
        do {
            encode.outputFormatting = .prettyPrinted
            jsonValue = try encode.encode(self)
        }
        catch let error {
            print("toJson -> \(error)")
        }
        if jsonValue != nil {
            let jsonString = String(data: jsonValue!, encoding: .utf8)
            return jsonString ?? ""
        }
        return ""
    }
}
struct UserFriendsAddReponse: Codable {
    var success: Bool
    var id: Int
    var friend: UserResponse
    
}
struct UserFriendDeleteReponse: Codable {
    var success: Bool
    var id: Int
    var friend: [UserResponse]
    
}
