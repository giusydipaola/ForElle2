//
//  SharedInfo.swift
//  Elle
//
//  Created by Gabriele Fioretti on 03/03/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import Foundation
import OneSignal

class SharedInfo: NSObject {
    
    //Variables
    public static let sharedInfo: SharedInfo = SharedInfo.init()                    //Permanente nell'applicazione
    var userData = UserDefaults.standard
    var user = UserApi()
    var helpRequestManager = HelpRequestManager()
    public static var API_URL = "https://api.forelle.app/"
    
    var tokenDevice: String {
        get {
            let token = OneSignal.getPermissionSubscriptionState()?.subscriptionStatus.userId ?? ""
            
            UserDefaults.standard.set(token, forKey: "tokenDevice")
            
            return token //UserDefaults.standard.string(forKey: "tokenDevice") ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "tokenDevice")
        }
    }
}
