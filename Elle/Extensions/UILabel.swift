//
//  UILabel.swift
//  Elle
//
//  Created by Giusy Di Paola on 22/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import Foundation
import UIKit


extension UILabel {
    func pushUp(_ text: String?) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.push
        animation.subtype = CATransitionSubtype.fromTop
        animation.duration = 0.5
        self.layer.add(animation,  forKey: CATransitionType.push.rawValue)
        self.text = text
    }
    
    // Animation fade in - fade out [LABELS]
        func fadeViewInThenOut(view : UILabel, delay: TimeInterval) {
            let animationDuration = 2.3
            UIView.animate(withDuration: animationDuration, delay: delay, options: [UIView.AnimationOptions.autoreverse, UIView.AnimationOptions.repeat], animations: {
                view.alpha = 0
            }, completion: nil)
        }
    }

