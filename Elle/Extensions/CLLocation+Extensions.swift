//
//  CLLocation+Extensions.swift
//  Elle
//
//  Created by Andrea Garau on 13/03/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import MapKit

extension CLLocation {

    /// Get distance between two points
    ///
    /// - Parameters:
    ///   - from: first point
    ///   - to: second point
    /// - Returns: the distance in meters
    class func distance(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> CLLocationDistance {
        let from = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let to = CLLocation(latitude: to.latitude, longitude: to.longitude)
        return from.distance(from: to)
    }
}
