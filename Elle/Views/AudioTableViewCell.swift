//
//  AudioTableViewCell.swift
//  Elle
//
//  Created by Andrea Garau on 22/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit
import AVFoundation

// play button, stop button, url, data, date
class AudioFragment {
    var audioURL: URL
    var creationDate: Date?
    
    init(audioURL: URL, creationDate: Date?) {
        self.audioURL = audioURL
        self.creationDate = creationDate
    }
}

/// Play an audio received by the requester
class AudioTableViewCell: UITableViewCell {
        
    //MARK: - IBOutlets
    @IBOutlet weak var audioDescription: UILabel!
    @IBOutlet weak var playButton: UIButton!
    
    public var helpRequestDetail: HelpRequestDetailsResponse?
    public var audioFragment: AudioFragment?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configure(helpRequestDetail: HelpRequestDetailsResponse) {
        self.helpRequestDetail = helpRequestDetail
        let date = Utils.stringToDate(string: helpRequestDetail.dateInsert)
        self.audioDescription.text = date != nil ? Utils.dateToString(date: date!) : "Audio"
    }
    
    public func configure(audioFragment: AudioFragment) {
        self.audioFragment = audioFragment
        self.audioDescription.text = audioFragment.creationDate != nil ? Utils.dateToString(date: audioFragment.creationDate!) : NSLocalizedString("NO_INFO", comment: "Nessuna informazione disponibile")
    }
    
}

